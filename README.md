

# README 

Este arquivo é para uma melhor compreensão de consumo da Api .

### Processo de instalação

Para realizar o deploy da API basta seguir os passos 

1. Docker
- Temos 3 arquivos de configuração do nossos containers em docker.
	- Dentro da pasta rais do projeto `\builders-teste\processoSeletivo\processoSeletivo`
	temos um arquivo com o arquivo com o nome `Dockerfile`, neste arquivo contém a versão do: `FROM maven:3.6.1-jdk-11-slim`.
	- Dentro da pasta raiz `\processoSeletivo` temos mais um arquivo `Dockerfile` com a configuração `FROM mysql:5.7.22 EXPOSE 3308` que contem a versão do MySql e a porta acessada podo docker-compose.
	- E por fim dentro desta mesma pasta raiz `\processoSeletivo`  temos o nosso arquivo `docker-compose`.
	Neste arquivo contém todas as configurações dos nossos containers, tanto para a Api quanto para o banco este é o arquivo que fará o orquestramento dos nossos containers

`

    version: '3.4'
    services:
      db:
        command: mysqld --default-authentication-plugin=mysql_native_password
        restart: always
        build:
          context: .
          dockerfile: Dockerfile
        environment:
          TZ: America/Sao_Paulo
          MYSQL_ROOT_PASSWORD: Admin357/
          MYSQL_USER: root
          MYSQL_PASSWORD: Admin357/
          MYSQL_DATABASE: db_builders      
        ports:
          - "3308:3306"
        networks:
          - builders-network
      processo_seletivo-api:
        restart: always
        build: ./processoSeletivo
        working_dir: /processoSeletivo
        environment:
          TZ: America/Sao_Paulo
          SPRING_BOOT_ENVIRONMENT: Production
        volumes:
          - ./processoSeletivo:/processoSeletivo
          - ~/.m2:/root/.m2
        ports:
          - "9000:8080"
        command: mvn clean spring-boot:run
        links:
          - db
        depends_on:
          - db
        networks:
          - builders-network 
    networks:
      builders-network:
            driver: bridge`
	

2. Uma vez que conseguimos entender isto, basta abrir os console e digitar os códigos abaixo.

- Deploy da aplicação: 
		- Navegue até pasta do pasta raiz `\builders-teste\processoSeletivo` insira o comando `docker-compose up -d --build`.

3. Alguns comandos que podem ser útil.

|  Comando|  Ação|
|--|--|
| `docker ps`  |  Lista todas ao containers |
| `docker logs {CONTAINER ID}` | Visualizar a saida console da aplicação
|`docker stop {CONTAINER ID}`|Pausa o container|
|  `docker rm {CONTAINER ID}`|  remove container pausado|
|  `docker start {CONTAINER ID}`|  Inicia o container que esta pausado|


Uma vez a aplicação no ar basta consumi-la utilizando o arquivo para consumo postman contido na raiz do repositório `\builders-teste`

***Muito obrigado***


