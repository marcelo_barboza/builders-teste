package com.builders.processoSeletivo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.builders.processoSeletivo.entity.Client;
import com.builders.processoSeletivo.entity.ClientPartial;
import com.builders.processoSeletivo.service.ClientService;

@RestController
@RequestMapping("/client")
@CrossOrigin("*")
public class ClientController {

	@Autowired
	private ClientService business;

	@GetMapping
	public ResponseEntity<Page<Client>> GetAll(int page, int size) {
		return ResponseEntity.ok(business.findAll(PageRequest.of(page, size)));
	}

	@GetMapping("/{id}")
	public ResponseEntity<Client> GetById(@PathVariable long id) {
		return business.findById(id).map(cliente -> ResponseEntity.ok(cliente))
				.orElse(ResponseEntity.notFound().build());
	}

	@GetMapping("/cpf/{cpf}")
	public ResponseEntity<Client> GetByCpf(@PathVariable long cpf) {
		return business.findByCpf(cpf).map(cliente -> ResponseEntity.ok(cliente))
				.orElse(ResponseEntity.notFound().build());
	}

	@GetMapping("/name/{name}")
	public ResponseEntity<Page<Client>> GetByNome(@PathVariable String name, int page, int size) {
		return ResponseEntity.ok(business.findAllByNameContainingIgnoreCase(PageRequest.of(page, size), name));
	}

	@PostMapping
	public ResponseEntity<Client> Post(@RequestBody Client client, UriComponentsBuilder uri) {

		Client cli = business.save(client);
		UriComponents uriComponents = uri.path("/client/{id}").buildAndExpand(cli.getId());

		return ResponseEntity.created(uriComponents.toUri()).body(cli);
	}

	@PutMapping
	public ResponseEntity<Client> Put(@RequestBody Client client) {
		return ResponseEntity.ok(business.save(client));
	}

	@PatchMapping("/{id}")
	public ResponseEntity<?> Patch(@RequestBody ClientPartial clientPartial, @PathVariable long id) {
		
		try {
			if(clientPartial.getDateOfBirth() == null && StringUtils.isEmpty(clientPartial.getName())) 
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("One of the two fields needs to be filled.");
			else
				return ResponseEntity.ok(business.partialUpdate(clientPartial, id));
			
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
		}
		
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<String> Delete(@PathVariable long id) {
		boolean clientIsPresent = business.findById(id).isPresent();

		if (clientIsPresent) {
			business.delete(id);
			return ResponseEntity.noContent().build();
		} else
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Resource not found.");
	}

}
