package com.builders.processoSeletivo.service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.builders.processoSeletivo.entity.Client;
import com.builders.processoSeletivo.entity.ClientPartial;

public interface ClientService {

	public Optional<Client> findByCpf (long cpf);
	
	public Page<Client> findAllByNameContainingIgnoreCase (Pageable page, String name);
	
	public Page<Client> findAll (Pageable page);
	
	public Optional<Client> findById (long id);
	
	public Client save (Client client);
	
	public Client partialUpdate (ClientPartial clientPartial, long id);
	
	public void delete (long id);
	
}
