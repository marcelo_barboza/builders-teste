package com.builders.processoSeletivo.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.builders.processoSeletivo.entity.Client;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {
	public Optional<Client> findByCpf (long cpf);
	public Page<Client> findAllByNameContainingIgnoreCase (Pageable page, String nome);

}
