package com.builders.processoSeletivo.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ClientPartial {
	
	@JsonProperty("nome")
	private String name;
	
	@JsonFormat(pattern = "yyyy-MM-dd")
	@JsonProperty("dataNascimento")
	private Date dateOfBirth;

	public String getName() {
		return name;
	}

	public void setName(String nome) {
		this.name = nome;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
	

}
