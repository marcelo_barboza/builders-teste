package com.builders.processoSeletivo.service.implementation;

import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.builders.processoSeletivo.entity.Client;
import com.builders.processoSeletivo.entity.ClientPartial;
import com.builders.processoSeletivo.repository.ClientRepository;
import com.builders.processoSeletivo.service.ClientService;

@Service
public class ClientServiceImpl implements ClientService {

	@Autowired
	private ClientRepository repository;

	@Override
	public Optional<Client> findByCpf(long cpf) {
		Optional<Client> client = repository.findByCpf(cpf);

		if (!client.isPresent())
			return client;

		client.get().setAge(GetAge(client.get().getDateOfBirth()));

		return client;
	}
	
	@Override
	public Page<Client> findAllByNameContainingIgnoreCase(Pageable page, String name) {
		Page<Client> client = repository.findAllByNameContainingIgnoreCase(page, name);
		
		if(!client.hasContent())
			return client;
		
		for (Client cli : client.getContent()) {
			cli.setAge(GetAge(cli.getDateOfBirth()));
		}		
		
		return client;
	}
	
	@Override
	public Page<Client> findAll(Pageable page) {
		Page<Client> client = repository.findAll(page);
		
		for (Client cli : client) 
			cli.setAge(GetAge(cli.getDateOfBirth()));
		
		return client;
	}
	
	@Override
	public Optional<Client> findById(long id) {
		Optional<Client> cliente = repository.findById(id);
		
		if(!cliente.isPresent())
			return cliente;
		
		cliente.get().setAge(GetAge(cliente.get().getDateOfBirth()));
		
		return cliente;
	}
	
	@Override
	public Client save(Client client) {		
		return repository.save(client);
	}
	
	@Override
	public Client partialUpdate(ClientPartial clientPartial, long id) {
		Client client = this.repository.findById(id).get();
		
		if(!StringUtils.isEmpty(clientPartial.getName()))
			client.setName(clientPartial.getName());
		
		if(clientPartial.getDateOfBirth() != null)
			client.setDateOfBirth(clientPartial.getDateOfBirth());		
		
		return this.repository.save(client);
	}

	@Override
	public void delete(long id) {
		repository.deleteById(id);

	}

	public static int GetAge(Date dataNasc) {

		Calendar dateOfBirth = Calendar.getInstance();
		dateOfBirth.setTime(dataNasc);
		Calendar today = Calendar.getInstance();

		int age = today.get(Calendar.YEAR) - dateOfBirth.get(Calendar.YEAR);

		if (today.get(Calendar.MONTH) < dateOfBirth.get(Calendar.MONTH)) {
			age--;
		} else {
			if (today.get(Calendar.MONTH) == dateOfBirth.get(Calendar.MONTH)
					&& today.get(Calendar.DAY_OF_MONTH) < dateOfBirth.get(Calendar.DAY_OF_MONTH)) {
				age--;
			}
		}

		return age;
	}
}
